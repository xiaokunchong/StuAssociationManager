const express = require("express");
const router = express.Router();
const path = require("path");
const multer = require("multer");

const storage = multer.diskStorage({
  destination: function (req, file, cb) {
    cb(null, path.resolve(__dirname, "./../../uploads"));
  },
  filename: function (req, file, cb) {
    const fileFormat = file.originalname.split(".");
    cb(null, Date.now() + "." + fileFormat[fileFormat.length - 1]);
  },
});
const upload = multer({ storage });

router.post("/uploads/images", upload.single("file"), async (req, res) => {
  const { filename } = req.file;
  return res.send({ filename });
});

module.exports = router;
