const mongoose = require("mongoose");

const schema = new mongoose.Schema(
    {
        photo: {type:String},
        name:{type:String},
        job:{type:String},
        email:{type:String},
        mobile:{type:String},
        sort:{type:Number,default:0}
    },
    {
        timestamps: { createdAt: "created", updatedAt: "updated" },
    }
);

module.exports = mongoose.model("contacts", schema);
