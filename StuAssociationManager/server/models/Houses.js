const mongoose = require("mongoose");

const schema = new mongoose.Schema(
    {
        type: { type: Number },
        bedroom: { type: Number },
        bathroom: { type: Number },
        carspace: { type: Number },
        price: { type: String },
        // priceType: { type: Number },
        description: { type: String },
        images: { type: Array },
        name: { type: String },
        // youtube: {type:String}
    },
    {
        timestamps: { createdAt: "created", updatedAt: "updated" },
    }
);

module.exports = mongoose.model("houses", schema);
