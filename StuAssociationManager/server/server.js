const { basic } = require("./config");
const express = require("express");
const app = express();
//引入依赖项
var bodyParser = require("body-parser");
app.use(bodyParser.json({ limit: "50mb" }));
app.use(bodyParser.urlencoded({ limit: "50mb", extended: true }));
//引入依赖，解决跨域
app.use(require("cors")());
require("./utils/mysql")
require("./routes/client")(app);
// require("./routes/controller")(app);

app.use("/images", express.static(__dirname + "/uploads"));
// app.use("/controller", express.static(__dirname + "/controller"));
app.use("/downloads", express.static(__dirname + "/downloads"));
app.use("/", express.static(__dirname + "/client"));

app.listen(basic.port, () => {
  console.log(`${basic.name} app listening on prot ${basic.port}`);
});
