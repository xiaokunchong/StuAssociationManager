module.exports = {
  basic: {
    get name() {
      return "community";
    },
    get port() {
      return 21505;
    }
  },
  mysql: {
    host: 'localhost',
    user: 'root',
    password: '123456',
    database: 'organization',
    // timezone: 'utc',
    // timezone: '+08:00'
  }
};