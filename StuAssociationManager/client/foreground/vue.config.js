// import npm path module for resolve the path
const path = require('path')
  // import the application default setting from settings.js
const defaultSetting = require('./src/settings.js')

// resolve function for resolve the path
function resolve(dir) {
  return path.join(__dirname, dir)
}

// set the application title
const name = defaultSetting.title || 'has not set'
  // configure the default running port number
const port = process.env.port || process.env.npm_config_port || 9999

// module interface exposure method
module.exports = {
  // visit a website should add the public path after url
  publicPath: '/',
  // the output path of the packaged files
  outputDir: 'dist',
  // the path of the packaged files for the static files
  assetsDir: 'static',
  // for eslint
  lintOnSave: false,
  // The production environment does not need, map file is for debugging
  productionSourceMap: false,
  // development server
  devServer: {
    port,
    open: true,
    // tips overlay the screen
    overlay: {
      warnings: false,
      errors: true
    }
  },
  configureWebpack: {
    // provide the app's title in webpack's name field, so that
    // it can be accessed in index.html to inject the correct title.
    name,
    resolve: {
      alias: {
        '@': resolve('src')
      }
    }
  },
  // packaing optimization
  chainWebpack(config) {
    // it can improve the speed of the first screen, it is recommended to turn on preload
    config.plugin('preload').tap(() => [{
        rel: 'preload',
        fileBlacklist: [/\.map$/, /hot-update\.js$/, /runtime\..*\.js$/],
        include: 'initial'
      }])
      // when there are many pages, it will cause too many meaningless requests
    config.plugins.delete('prefetch')
      // svg icon
    config.module.rule('svg').exclude.add(resolve('src/svgs')).end()
    config.module.rule('svgs').test(/\.svg$/).include.add(resolve('src/svgs')).end().use('svg-sprite-loader').loader('svg-sprite-loader').options({
      symbolId: 'icon-[name]'
    })
  }
}