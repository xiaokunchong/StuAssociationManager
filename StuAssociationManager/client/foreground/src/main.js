import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'

import http from '@/utils/request'
import moment from 'moment'
import ElementUI from 'element-ui'
import 'element-ui/lib/theme-chalk/index.css'

Vue.prototype.$moment = moment
// import 'vant/lib/index.css'
// import './styles/index.scss'
import '@/plugins/icon'
import 'swiper/dist/css/swiper.css'

// import '@/utils/rem'

// Vue.use(Vant)
Vue.use(ElementUI)
// Vue.use(VueAwesomeSwiper)
Vue.prototype.$http = http

Vue.config.productionTip = false

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')