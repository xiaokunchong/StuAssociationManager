import Vue from 'vue'
import VueRouter from 'vue-router'
import Home from '../views/Home.vue'

Vue.use(VueRouter)

const routes = [{
    path: '/',
    name: 'Home',
    component: Home
  },
  {
    path: '/activityList',
    name: 'ActivityList',
    component: () =>
      import ('@/views/ActivityList')
  },
  {
    path: '/login',
    name: 'Login',
    component: () =>
      import ('@/views/Login')
  },
  {
    path: '/register',
    name: 'Register',
    component: () =>
      import ('@/views/Register')
  },
  {
    path: '/comment',
    name: 'Comment',
    props: true,
    component: () =>
      import ('@/views/Comment')
  },
  {
    path: '/organization',
    name: 'Organization',
    component: () =>
      import ('@/views/Organization')
  },
  {
    path: '/organization-detail/:id',
    props: true,
    name: 'OrganizationDetail',
    component: () =>
      import ('@/views/OrganizationDetail')
  },
  {
    path: '/announce-detail/:id',
    props: true,
    name: 'AnnounceDetail',
    component: () =>
      import ('@/views/AnnounceDetail')
  },
  {
    path: '/activity-detail/:id',
    props: true,
    name: 'ActivityDetail',
    component: () =>
      import ('@/views/ActivityDetail')
  },
  {
    path: '/announce-list',
    props: true,
    name: 'AnnounceList',
    component: () =>
      import ('@/views/AnnounceList')
  },
  {
    path: '/personal',
    name: 'Personal',
    component: () =>
      import ('@/views/Personal')
  },
  {
    path: '/PersonalInfo',
    name: 'PersonalInfo',
    component: () =>
      import ('@/views/PersonalInfo')
  }
]

const router = new VueRouter({
  routes
})
const originalPush = VueRouter.prototype.push
VueRouter.prototype.push = function push(location) {
  return originalPush.call(this, location).catch(err => err)
}

router.beforeEach((to, from, next) => {
  document.documentElement.scrollTop = 0
  next()
})
export default router