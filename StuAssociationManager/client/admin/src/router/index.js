import Vue from "vue";
import Router from "vue-router";

Vue.use(Router);

/* Layout */
import Layout from "@/layout";

/**
 * Note: sub-menu only appear when route children.length >= 1
 * Detail see: https://panjiachen.github.io/vue-element-admin-site/guide/essentials/router-and-nav.html
 *
 * hidden: true                   if set true, item will not show in the sidebar(default is false)
 * alwaysShow: true               if set true, will always show the root menu
 *                                if not set alwaysShow, when item has more than one children route,
 *                                it will becomes nested mode, otherwise not show the root menu
 * redirect: noRedirect           if set noRedirect will no redirect in the breadcrumb
 * name:'router-name'             the name is used by <keep-alive> (must set!!!)
 * meta : {
    roles: ['admin','editor']    control the page roles (you can set multiple roles)
    title: 'title'               the name show in sidebar and breadcrumb (recommend set)
    icon: 'svg-name'/'el-icon-x' the icon show in the sidebar
    breadcrumb: false            if set false, the item will hidden in breadcrumb(default is true)
    activeMenu: '/example/list'  if set path, the sidebar will highlight the path you set
  }
 */

/**
 * constantRoutes
 * a base page that does not have permission requirements
 * all roles can be accessed
 */
export const constantRoutes = [
  // 添加社团
  {
    path: "/organization",
    component: Layout,
    children: [{
      path: "/organization/list",
      component: () =>
        import("@/views/organization/list"),
      meta: {
        title: "社团列表",
        icon: "el-icon-setting"
      }
    },
    {
      path: "/organization/OrgInfo",
      component: () =>
        import("@/views/organization/OrgInfo"),
      meta: {
        title: "社团信息",
        icon: "el-icon-setting",
        shetuan: true
      }
    }
      // , {
      //   path: '/organization/list',
      //   component: () =>
      //     import ('@/views/organization/list'),
      //   meta: {
      //     title: '社团信息',
      //     icon: 'el-icon-setting'
      //   }
      // }
    ],
    meta: {
      title: "社团管理",
      icon: "el-icon-setting"
    }
    // hidden: true,
  },
  // 首页
  // {
  //   path: '/first',
  //   component: Layout,
  //   children: [{
  //     path: '/first-page',
  //     component: () =>
  //       import ('@/views/first/first-page'),
  //     meta: {
  //       title: '首页设置',
  //       icon: 'el-icon-setting'
  //     }
  //   }]
  // },
  // 用户列表
  {
    path: "/Permission",
    component: Layout,
    children: [

      {
        path: "/Permission/ChangePassword",
        component: () =>
          import("@/views/Permission/ChangePassword"),
        meta: {
          title: "修改密码",
          icon: "el-icon-user"
        }
      },
      {
        path: "/Permission/list",
        component: () =>
          import("@/views/Permission/list"),
        meta: {
          title: "社团管理员管理",
          icon: "el-icon-user",
          shetuan: true
        }
      },
      {
        path: "/Permission/list1",
        component: () =>
          import("@/views/Permission/list1"),
        meta: {
          title: "社团管理员管理",
          icon: "el-icon-user"
        }
      }
    ],
    meta: {
      title: "权限管理",
      icon: "el-icon-user"
    }
  },
  // 审核列表
  {
    path: "/UserManage",
    component: Layout,
    children: [{
      path: "/UserManage/list",
      component: () =>
        import("@/views/UserManage/list"),
      meta: {
        title: "注册用户信息",
        icon: "el-icon-user"
      }
    }, {
      path: "/UserManage/MemberInfo",
      component: () =>
        import("@/views/UserManage/MemberInfo"),
      meta: {
        title: "社团成员信息",
        icon: "el-icon-user",
        shetuan: true,
      }
    }, {
      path: "/UserManage/VipInfo",
      component: () =>
        import("@/views/UserManage/VipInfo"),
      meta: {
        title: "社团会员信息",
        icon: "el-icon-user",
        shetuan: true,
      }
    }],
    meta: {
      title: "用户管理",
      icon: "el-icon-user"
    }
  },
  // 发布管理
  {
    path: "/AnnounceManage",
    component: Layout,
    children: [{
      path: "/AnnounceManage/ClubRecruit",
      component: () =>
        import("@/views/AnnounceManage/ClubRecruit"),
      meta: {
        title: "社团招新",
        icon: "el-icon-setting",
        shetuan: true
      }
    },
    {
      path: "/AnnounceManage/ClubActivity",
      component: () =>
        import("@/views/AnnounceManage/ClubActivity"),
      meta: {
        title: "社团活动",
        icon: "el-icon-setting",
        shetuan: true
      }
    }, {
      path: "/AnnounceManage/AfficheInfo",
      component: () =>
        import("@/views/AnnounceManage/AfficheInfo"),
      meta: {
        title: "社团公告信息",
        icon: "el-icon-user",
        shetuan: true,
      }
    }, {
      path: "/AnnounceManage/ClubAnnouncement",
      component: () =>
        import("@/views/AnnounceManage/ClubAnnouncement"),
      meta: {
        title: "系统公告",
        icon: "el-icon-setting"
      }
    },
    ],
    meta: {
      title: "发布管理",
      icon: "el-icon-setting"
    }
  },
  {
    path: '/comment/list',
    meta: {
      title: '留言管理',
      icon: 'el-icon-setting',
    },
    component: Layout,
    children: [{
      path: '/comment/list',
      component: () =>
        import('@/views/comment/list'),
      meta: {
        title: '留言列表',
        icon: 'el-icon-setting'
      },
    }]
  }, {
    path: "/audit",
    component: Layout,
    children: [{
      path: "/audit/OrgApply",
      component: () =>
        import("@/views/audit/OrgApply"),
      meta: {
        title: "新社团创建申请",
        icon: "el-icon-user"
      }
    },
    {
      path: "/audit/VipApply",
      component: () =>
        import("@/views/audit/VipApply"),
      meta: {
        title: "会员申请",
        icon: "el-icon-setting",
        shetuan: true,
      },
    },
    {
      path: "/audit/PositionApply",
      component: () =>
        import("@/views/audit/PositionApply"),
      meta: {
        title: "职位申请",
        icon: "el-icon-setting",
        shetuan: true,
      },
    },
    {
      path: "/audit/QuitApply",
      component: () =>
        import("@/views/audit/QuitApply"),
      meta: {
        title: "退出社团申请",
        icon: "el-icon-setting",
        shetuan: true,
      },
    },
    {
      path: "/audit/UserActivityApply",
      component: () =>
        import("@/views/audit/UserActivityApply"),
      meta: {
        title: "参加活动申请",
        icon: "el-icon-setting",
        shetuan: true,
      },
    },
    ],
    meta: {
      title: "审核管理",
      icon: "el-icon-user"
    }
  },

  {
    path: "/classification",
    component: Layout,
    children: [{
      path: "/TypeManage/FacMajClass",
      component: () =>
        import("@/views/TypeManage/FacMajClass"),
      meta: {
        title: "学院",
        icon: "el-icon-setting"
      }
    },
    {
      path: "/TypeManage/Major",
      component: () =>
        import("@/views/TypeManage/Major"),
      meta: {
        title: "专业",
        icon: "el-icon-setting"
      }
    },
    {
      path: "/TypeManage/Class",
      component: () =>
        import("@/views/TypeManage/Class"),
      meta: {
        title: "班级",
        icon: "el-icon-setting"
      }
    },
    {
      path: "/TypeManage/ClubType",
      component: () =>
        import("@/views/TypeManage/ClubType"),
      meta: {
        title: "社团类别",
        icon: "el-icon-setting"
      }
    },
    {
      path: "/TypeManage/BranchType",
      component: () =>
        import("@/views/TypeManage/BranchType"),
      meta: {
        title: "部门类别",
        icon: "el-icon-setting",
        shetuan: true
      }
    },
    {
      path: "/TypeManage/PositionType",
      component: () =>
        import("@/views/TypeManage/PositionType"),
      meta: {
        title: "职位类别",
        icon: "el-icon-setting"
      }
    }
    ],
    meta: {
      title: "类别管理",
      icon: "el-icon-setting"
    }
  },
  {
    path: "/login",
    component: () =>
      import("@/views/login/index"),
    hidden: true
  },
  {
    path: "/404",
    component: () =>
      import("@/views/404"),
    hidden: true
  },
  {
    path: "/",
    redirect: "/organization/list"
  },
  // 404 page must be placed at the end !!!
  { path: "*", redirect: "/404", hidden: true }
];

const createRouter = () =>
  new Router({
    // mode: 'history', // require service support
    scrollBehavior: () => ({ y: 0 }),
    routes: constantRoutes
  });

const router = createRouter();


export function resetRouter() {
  const newRouter = createRouter();
  router.matcher = newRouter.matcher; // reset router
}

export default router;