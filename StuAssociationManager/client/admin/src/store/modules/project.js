import {
  addInvestor,
  addNotification,
  addProject,
  addService,
  adminAddEnterprise,
  adminAddInvestor,
  adminAddPersonal,
  changeInfo,
  clearUnread,
  deleteAutomatic,
  deleteNotification,
  deleteProject,
  deleteProjectById,
  deleteService,
  getAutomatic,
  getChatRecords,
  getInviteCode,
  getMember,
  getNotification,
  getPeopleList,
  getProjectByUser,
  getService,
  getServiceById,
  getUserByGovernment,
  push,
  pushs,
  searchUserForProject,
  setAutomatic,
  setInviteCode,
  setNotification,
  setProject,
  setService,
  setUserTag
} from '@/api/project'

const {
  getSkin,
  setSkin,
  addSkin,
  deleteSkin,
  getArticle,
  setArticle,
  addArticle,
  deleteArticle,
  getStatic,
  setStatic,
  addStatic,
  deleteStatic,
  getBanner,
  setBanner,
  addBanner,
  deleteBanner,
  addGame,
  setGame,
  getGame,
  deleteGame,
  getUser,
  setUserMember,
  deleteUser,
  getProject,
  getNewsType,
  setNewsType,
  addNewsType,
  deleteNewsType,
  setNews,
  addNews,
  deleteNews,
  getNews,
  getUserById,
  getUserAudit,
  setUserBaseInfo,
  setUserAudit
} = require('@/api/project')

const actions = {
  getSkin({ commit }, data) {
    return new Promise((resolve, reject) => {
      getSkin(data)
        .then(res => resolve(res))
        .catch(err => reject(err))
    })
  },
  addSkin({ commit }, data) {
    return new Promise((resolve, reject) => {
      addSkin(data)
        .then(res => resolve(res))
        .catch(err => reject(err))
    })
  },
  setSkin({ commit }, data) {
    return new Promise((resolve, reject) => {
      setSkin(data)
        .then(res => resolve(res))
        .catch(err => reject(err))
    })
  },
  deleteSkin({ commit }, data) {
    return new Promise((resolve, reject) => {
      deleteSkin(data)
        .then(res => resolve(res))
        .catch(err => reject(err))
    })
  },
  getArticle({ commit }, data) {
    return new Promise((resolve, reject) => {
      getArticle(data)
        .then(res => resolve(res))
        .catch(err => reject(err))
    })
  },
  setArticle({ commit }, data) {
    return new Promise((resolve, reject) => {
      setArticle(data)
        .then(res => resolve(res))
        .catch(err => reject(err))
    })
  },
  addArticle({ commit }, data) {
    return new Promise((resolve, reject) => {
      addArticle(data)
        .then(res => resolve(res))
        .catch(err => reject(err))
    })
  },
  deleteArticle({ commit }, data) {
    return new Promise((resolve, reject) => {
      deleteArticle(data)
        .then(res => resolve(res))
        .catch(err => reject(err))
    })
  },
  getStatic() {
    return new Promise((resolve, reject) => {
      getStatic()
        .then(res => resolve(res))
        .catch(err => reject(err))
    })
  },
  setStatic({ commit }, data) {
    return new Promise((resolve, reject) => {
      setStatic(data)
        .then(res => resolve(res))
        .catch(err => reject(err))
    })
  },
  addStatic({ commit }, data) {
    return new Promise((resolve, reject) => {
      addStatic(data)
        .then(res => resolve(res))
        .catch(err => reject(err))
    })
  },
  deleteStatic({ commit }, data) {
    return new Promise((resolve, reject) => {
      deleteStatic(data)
        .then(res => resolve(res))
        .catch(err => reject(err))
    })
  },
  getBanner({ commit }, data) {
    return new Promise((resolve, reject) => {
      getBanner(data)
        .then(res => resolve(res))
        .catch(err => reject(err))
    })
  },
  setBanner({ commit }, data) {
    return new Promise((resolve, reject) => {
      setBanner(data)
        .then(res => resolve(res))
        .catch(err => reject(err))
    })
  },
  addBanner({ commit }, data) {
    return new Promise((resolve, reject) => {
      addBanner(data)
        .then(res => resolve(res))
        .catch(err => reject(err))
    })
  },
  deleteBanner({ commit }, data) {
    return new Promise((resolve, reject) => {
      deleteBanner(data)
        .then(res => resolve(res))
        .catch(err => reject(err))
    })
  },
  addGame({ commit }, data) {
    return new Promise((resolve, reject) => {
      addGame(data)
        .then(res => resolve(res))
        .catch(err => reject(err))
    })
  },
  setGame({ commit }, data) {
    return new Promise((resolve, reject) => {
      setGame(data)
        .then(res => resolve(res))
        .catch(err => reject(err))
    })
  },
  getGame({ commit }, data) {
    return new Promise((resolve, reject) => {
      getGame(data)
        .then(res => resolve(res))
        .catch(err => reject(err))
    })
  },
  deleteGame({ commit }, data) {
    return new Promise((resolve, reject) => {
      deleteGame(data)
        .then(res => resolve(res))
        .catch(err => reject(err))
    })
  },
  getUser({ commit }, data) {
    return new Promise((resolve, reject) => {
      getUser(data)
        .then(res => resolve(res))
        .catch(err => reject(err))
    })
  },
  getUserAudit({ commit }, data) {
    return new Promise((resolve, reject) => {
      getUserAudit(data)
        .then(res => resolve(res))
        .catch(err => reject(err))
    })
  },
  setUserAudit({ commit }, data) {
    return new Promise((resolve, reject) => {
      setUserAudit(data)
        .then(res => resolve(res))
        .catch(err => reject(err))
    })
  },
  getUserById({ commit }, data) {
    return new Promise((resolve, reject) => {
      getUserById(data)
        .then(res => resolve(res))
        .catch(err => reject(err))
    })
  },
  setUserTag({ commit }, data) {
    return new Promise((resolve, reject) => {
      setUserTag(data)
        .then(res => resolve(res))
        .catch(err => reject(err))
    })
  },
  setUserMember({ commit }, data) {
    return new Promise((resolve, reject) => {
      setUserMember(data)
        .then(res => resolve(res))
        .catch(err => reject(err))
    })
  },
  setUserBaseInfo({ commit }, data) {
    return new Promise((resolve, reject) => {
      setUserBaseInfo(data)
        .then(res => resolve(res))
        .catch(err => reject(err))
    })
  },
  deleteUser({ commit }, data) {
    return new Promise((resolve, reject) => {
      deleteUser(data)
        .then(res => resolve(res))
        .catch(err => reject(err))
    })
  },
  getProjectByUser({ commit }, data) {
    return new Promise((resolve, reject) => {
      getProjectByUser(data)
        .then(res => resolve(res))
        .catch(err => reject(err))
    })
  },
  getProject({ commit }, data) {
    return new Promise((resolve, reject) => {
      getProject(data)
        .then(res => resolve(res))
        .catch(err => reject(err))
    })
  },
  deleteProjectById({ commit }, data) {
    return new Promise((resolve, reject) => {
      deleteProjectById(data)
        .then(res => resolve(res))
        .catch(err => reject(err))
    })
  },
  // 新闻类型
  getNewsType({ commit }, data) {
    return new Promise((resolve, reject) => {
      getNewsType(data)
        .then(res => resolve(res))
        .catch(err => reject(err))
    })
  },
  setNewsType({ commit }, data) {
    return new Promise((resolve, reject) => {
      setNewsType(data)
        .then(res => resolve(res))
        .catch(err => reject(err))
    })
  },
  addNewsType({ commit }, data) {
    return new Promise((resolve, reject) => {
      addNewsType(data)
        .then(res => resolve(res))
        .catch(err => reject(err))
    })
  },
  deleteNewsType({ commit }, data) {
    return new Promise((resolve, reject) => {
      deleteNewsType(data)
        .then(res => resolve(res))
        .catch(err => reject(err))
    })
  },
  // 新闻
  getNews({ commit }, data) {
    return new Promise((resolve, reject) => {
      getNews(data)
        .then(res => resolve(res))
        .catch(err => reject(err))
    })
  },
  setNews({ commit }, data) {
    return new Promise((resolve, reject) => {
      setNews(data)
        .then(res => resolve(res))
        .catch(err => reject(err))
    })
  },
  addNews({ commit }, data) {
    return new Promise((resolve, reject) => {
      addNews(data)
        .then(res => resolve(res))
        .catch(err => reject(err))
    })
  },
  deleteNews({ commit }, data) {
    return new Promise((resolve, reject) => {
      deleteNews(data)
        .then(res => resolve(res))
        .catch(err => reject(err))
    })
  },
  // 客服
  addService({ commit }, data) {
    return new Promise((resolve, reject) => {
      addService(data)
        .then(res => resolve(res))
        .catch(err => reject(err))
    })
  },
  setService({ commit }, data) {
    return new Promise((resolve, reject) => {
      setService(data)
        .then(res => resolve(res))
        .catch(err => reject(err))
    })
  },
  getService({ commit }, data) {
    return new Promise((resolve, reject) => {
      getService(data)
        .then(res => resolve(res))
        .catch(err => reject(err))
    })
  },
  deleteService({ commit }, data) {
    return new Promise((resolve, reject) => {
      deleteService(data)
        .then(res => resolve(res))
        .catch(err => reject(err))
    })
  },
  // 查询一个用户所有通信记录根据用户id
  getPeopleList({ commit }, data) {
    return new Promise((resolve, reject) => {
      getPeopleList(data)
        .then(res => resolve(res))
        .catch(err => reject(err))
    })
  },
  getServiceById({ commit }, data) {
    return new Promise((resolve, reject) => {
      getServiceById(data)
        .then(res => resolve(res))
        .catch(err => reject(err))
    })
  },
  getChatRecords({ commit }, data) {
    return new Promise((resolve, reject) => {
      getChatRecords(data)
        .then(res => resolve(res))
        .catch(err => reject(err))
    })
  },
  // 聊天未读消息清0
  clearUnread({ commit }, data) {
    return new Promise((resolve, reject) => {
      clearUnread(data)
        .then(res => resolve(res))
        .catch(err => reject(err))
    })
  },
  // 添加投资者
  addInvestor({ commit }, data) {
    return new Promise((resolve, reject) => {
      addInvestor(data)
        .then(res => resolve(res))
        .catch(err => reject(err))
    })
  },
  adminAddInvestor({ commit }, data) {
    return new Promise((resolve, reject) => {
      adminAddInvestor(data)
        .then(res => resolve(res))
        .catch(err => reject(err))
    })
  },
  // 添加个人创业者
  adminAddPersonal({ commit }, data) {
    return new Promise((resolve, reject) => {
      adminAddPersonal(data)
        .then(res => resolve(res))
        .catch(err => reject(err))
    })
  },
  // 添加企业创业者
  adminAddEnterprise({ commit }, data) {
    return new Promise((resolve, reject) => {
      adminAddEnterprise(data)
        .then(res => resolve(res))
        .catch(err => reject(err))
    })
  },
  // 添加项目
  addProject({ commit }, data) {
    return new Promise((resolve, reject) => {
      addProject(data)
        .then(res => resolve(res))
        .catch(err => reject(err))
    })
  },
  // 修改项目
  setProject({ commit }, data) {
    return new Promise((resolve, reject) => {
      setProject(data)
        .then(res => resolve(res))
        .catch(err => reject(err))
    })
  },
  // 删除项目
  deleteProject({ commit }, data) {
    return new Promise((resolve, reject) => {
      deleteProject(data)
        .then(res => resolve(res))
        .catch(err => reject(err))
    })
  },
  // 添加项目时搜索用户
  searchUserForProject({ commit }, data) {
    return new Promise((resolve, reject) => {
      searchUserForProject(data)
        .then(res => resolve(res))
        .catch(err => reject(err))
    })
  },
  // 查询投标用户根据政府项目id
  getUserByGovernment({ commit }, data) {
    return new Promise((resolve, reject) => {
      getUserByGovernment(data)
        .then(res => resolve(res))
        .catch(err => reject(err))
    })
  },
  // 每日邀请码
  getInviteCode() {
    return new Promise((resolve, reject) => {
      getInviteCode()
        .then(res => resolve(res))
        .catch(err => reject(err))
    })
  },
  setInviteCode({ commit }, data) {
    return new Promise((resolve, reject) => {
      setInviteCode(data)
        .then(res => resolve(res))
        .catch(err => reject(err))
    })
  },
  // 查询会员
  getMember({ commit }, data) {
    return new Promise((resolve, reject) => {
      getMember(data)
        .then(res => resolve(res))
        .catch(err => reject(err))
    })
  },
  // 通知功能
  getNotification({ commit }, data) {
    return new Promise((resolve, reject) => {
      getNotification(data)
        .then(res => resolve(res))
        .catch(err => reject(err))
    })
  },
  setNotification({ commit }, data) {
    return new Promise((resolve, reject) => {
      setNotification(data)
        .then(res => resolve(res))
        .catch(err => reject(err))
    })
  },
  addNotification({ commit }, data) {
    return new Promise((resolve, reject) => {
      addNotification(data)
        .then(res => resolve(res))
        .catch(err => reject(err))
    })
  },
  deleteNotification({ commit }, data) {
    return new Promise((resolve, reject) => {
      deleteNotification(data)
        .then(res => resolve(res))
        .catch(err => reject(err))
    })
  },
  // 全局推送
  pushs({ commit }, data) {
    return new Promise((resolve, reject) => {
      pushs(data)
        .then(res => resolve(res))
        .catch(err => reject(err))
    })
  },
  // 个推
  push({ commit }, data) {
    return new Promise((resolve, reject) => {
      push(data)
        .then(res => resolve(res))
        .catch(err => reject(err))
    })
  },
  getAutomatic({ commit }, data) {
    return new Promise((resolve, reject) => {
      getAutomatic(data)
        .then(res => resolve(res))
        .catch(err => reject(err))
    })
  },
  setAutomatic({ commit }, data) {
    return new Promise((resolve, reject) => {
      setAutomatic(data)
        .then(res => resolve(res))
        .catch(err => reject(err))
    })
  },
  deleteAutomatic({ commit }, data) {
    return new Promise((resolve, reject) => {
      deleteAutomatic(data)
        .then(res => resolve(res))
        .catch(err => reject(err))
    })
  },
  // 修改个人信息
  changeInfo({ commit }, data) {
    return new Promise((resolve, reject) => {
      changeInfo(data)
        .then(res => resolve(res))
        .catch(err => reject(err))
    })
  }
}

export default {
  actions,
  namespaced: true
}
