/**
 * 选择多文件,用于ajax 多文件上传
 * @param type 文件类型,支持[img,video,file]
 * @param multiple, 是否可多选
 */
export const selectMultifile = function(callable, type = 'image/*', multiple = true) {
  const input = document.createElement('input')
  // name="file" type="file" accept="image/*" @change="uploadChange"
  input.setAttribute('name', 'file')
  input.setAttribute('type', 'file')
  input.setAttribute('accept', type)
  if (multiple) {
    input.setAttribute('multiple', 'multiple')
  }
  input.style.display = 'none'
  // 添加到页面body元素上
  document.body.appendChild(input)
  input.click()
  input.addEventListener('change', function() {
    callable(input)
    // 从页面body上删除元素
    document.body.removeChild(input)
  })
}
