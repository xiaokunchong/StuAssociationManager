import { deepClone } from '@/utils'
import Json2excel from 'covert-json-to-excel'
import { Loading } from 'element-ui'
import { mapState } from 'vuex'

export default {
  created() {
    this.getData()
  },
  data() {
    return {
      // 查询数据,条件query
      query: {
        page: 1,
        pagesize: 15
      },
      tableLoading: false, // 表格是否显示在加载中,用于显示遮罩
      tableData: [], // 表格数据
      editData: {}, // 当前修改项数据
      addData: {}, // 当前添加项数据
      editshow: false, // 编辑弹框是否显示
      addshow: false, // 添加弹框是否显示
      // 分页组件配置项
      pagination: {
        props: {
          total: 0,
          background: true,
          'page-sizes': [15, 30, 50, 100],
          'layout': 'sizes,prev, pager, next'
        },
        events: {
          'current-change': (page) => {
            this.query.page = page
            this.getData()
          },
          'size-change': (size) => {
            this.query.pagesize = size
            this.getData()
          }
        }
      }
    }
  },
  methods: {
    // 获取数据列表
    async getData() {
      this.tableLoading = true
      const res = await this.$http[this.keys.getData](this.query)
      this.tableLoading = false
      // 遍历data对象取出数组
      for (const key in res.data) {
        if (res.data[key] instanceof Array) {
          this.tableData = res.data[key]
        }
        if (key === 'total' && typeof res.data[key] === 'number') {
          this.pagination.props.total = res.data[key]
        }
      }
    },
    // 导出excel 功能
    async exportExcel() {
      const query = { ...this.query }
      let jsonArray
      query.page = 1
      query.pagesize = 100000
      const loading = Loading.service({ background: 'rgba(0, 0, 0, 0.7)', spinner: 'el-icon-loading', text: '拼命加载中' })
      const res = await this.$http[this.keys.getData](query)
      // 遍历data对象取出数组
      for (const key in res.data) {
        if (res.data[key] instanceof Array) {
          jsonArray = res.data[key]
          break
        }
      }
      // 导出excel
      // Json2excel
      if (jsonArray && jsonArray.length) {
        new Json2excel({
          keyMap: this.excelheader, data: jsonArray,
          onStart: () => {
            // 开始
          }, onSuccess: () => {
            // 成功
            loading.close()
          }
        }).generate()
      }
    },
    // 编辑触发
    edit(obj) {
      this.editData = deepClone(obj)
    },
    // 保存编辑的数据
    async saveEdit(obj) {
      const res = await this.$http[this.keys.update](obj)
      if (res.code == 200) {
        // 更新成功
        this.$message({ message: '更新成功', type: 'success' })
        this.editshow = false
        // 重新获取数据
        this.getData()
        return true
      } else {
        // 更新失败
        this.$message.error('更新失败!')
        return false
      }
    },
    // 插入一条新的数据
    async insert(data) {
      const res = await this.$http[this.keys.insert](data)
      if (res.code === 200) {
        this.$message({ type: 'success', message: '添加成功OK' })
        this.addshow = false
        this.getData()
      }
      return true
    },
    /**
     * 删除,或者批量删除
     * @param ids [Array]
     */
    async remove(ids) {
      if (!this.auth) return
      const bool = await this.$confirm('此操作将删除该数据, 是否继续?', '提示', {
        confirmButtonText: '删除',
        cancelButtonText: '取消',
        type: 'warning'
      }).catch(() => {})
      if (bool) {
        const res = await this.$http[this.keys.remove](ids)
        if (res.message == '成功') {
          this.$message({ type: 'success', message: '删除成功!' })
          this.getData()
        } else {
          this.$message.error(res.message)
        }
      }
    }
  },
  computed: {
    // 当前用户是否有修改和删除权限,auth=true:有权限
    ...mapState({
      auth: state => state.user.userinfo.auth
    })
  }
}
