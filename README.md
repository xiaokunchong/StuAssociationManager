# 基于vue+nodejs的学生社团管理系统设计与实现

#### 介绍
本项目前端使用vue+elementui、后端使用node+express、数据库使用mysql，分前台网站和后台管理平台（根据角色又分系统管理员后台和社团管理员后台）  
（1）技术栈：  
前端：vue+elementui  
后端：nodejs+express  
数据库：mysql

（2）运行：  
①安装好npm、node  
②将数据库文件organization.sql导入myql数据库  
③用vscode打开项目  
④进入server文件夹：  
安装依赖：npm install  
启动：node server.js  
⑤进入admin文件夹：  
安装依赖： npm install  
启动：npm run dev  
⑥进入foreground文件夹：  
安装依赖：npm install  
启动：npm run dev

（3）本机系统配置：  
node版本：16.4.0  
npm版本：7.22.0  
vue脚手架版本：@vue/cli 4.5.13  
mysql版本：8.0.18

（4）后台管理员账号密码：
密码用md5加密了，可直接修改密码。  
系统管理员后台账号：admin      密码：a123456  
社团管理员后台账号（也可添加新的社团管理员账号，这里举个例子）：李小测   密码：a123456


#### 部分截图：
（1）前台网站部分截图
![前台首页](https://foruda.gitee.com/images/1670920633211332364/399b0b70_7873896.png "1.png")
![公告](https://foruda.gitee.com/images/1670920687528952902/4daa91ab_7873896.png "2.png")
![社团列表](https://foruda.gitee.com/images/1670920761003901652/fc44362f_7873896.png "3.png")
![社团介绍](https://foruda.gitee.com/images/1670920803395186931/827e9aca_7873896.png "4.png")
![留言板](https://foruda.gitee.com/images/1670920830112091318/c24800be_7873896.png "5.png")
![加入的社团](https://foruda.gitee.com/images/1670920860165792029/ab8a463b_7873896.png "6.png")

（2）后台管理平台部分截图
![社团管理员后台界面](https://foruda.gitee.com/images/1670920889592924439/d2a954ee_7873896.png "7.png")
![系统管理员后台界面](https://foruda.gitee.com/images/1670920923353243914/a65665a3_7873896.png "8.png")