/*
Navicat MySQL Data Transfer

Source Server         : localhost_3306
Source Server Version : 80018
Source Host           : localhost:3306
Source Database       : organization

Target Server Type    : MYSQL
Target Server Version : 80018
File Encoding         : 65001

Date: 2022-05-25 10:06:51
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for activity
-- ----------------------------
DROP TABLE IF EXISTS `activity`;
CREATE TABLE `activity` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '活动名称',
  `content` varchar(5000) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '活动内容',
  `icon` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '活动图片',
  `update_time` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `organization_id` int(11) NOT NULL COMMENT '举办部门',
  `create_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '发布时间',
  `activityObj` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '活动对象、招新对象',
  `timeStart` datetime(6) DEFAULT NULL COMMENT '活动开始时间',
  `timeEnd` datetime(6) DEFAULT NULL COMMENT '活动结束时间',
  `host` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '举办方',
  `activityPlace` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '活动地点',
  `enrollTime` datetime DEFAULT NULL,
  `activityObjtext` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '招新对象内容',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=37 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of activity
-- ----------------------------
INSERT INTO `activity` VALUES ('1', 'fsdfsadfsdffdsaf', '<p>fsdfsdafsdafsadfsdfsafsdf</p>', '1650896823938.png', null, '26', '2022-04-25 22:27:13', '招新对象', '2022-04-25 00:00:00.000000', '2022-04-26 00:00:00.000000', 'fdsafsadfsadfsda', 'fsdafsadfdsa', null, null);
INSERT INTO `activity` VALUES ('7', '这是测试参加活动', '<p>3123</p>', '1652001470163.jpg', null, '34', '2022-05-08 17:18:09', '活动对象', '2022-05-19 00:00:00.000000', '2022-05-28 00:00:00.000000', '3123', '313', null, null);
INSERT INTO `activity` VALUES ('9', '测试活动', '<p>彩色输出是</p>', '1652010788465.jpg', null, '24', '2022-05-08 19:53:22', '活动对象', '2022-05-10 00:00:00.000000', '2022-05-12 00:00:00.000000', '测试', '测试', null, null);
INSERT INTO `activity` VALUES ('10', '3123', '<p>231</p>', '1652010968753.jpg', null, '24', '2022-05-08 19:56:19', '招新对象', '2022-05-17 00:00:00.000000', '2022-05-26 00:00:00.000000', '123', '31', null, null);
INSERT INTO `activity` VALUES ('11', '军事爱好者灯会', '<p>军事爱好者灯会是由军事爱好者协会举办，包括猜灯谜等活动，欢迎大家参加</p>', '1652021597335.jpg', null, '38', '2022-05-08 22:55:31', '活动对象', '2022-05-11 00:00:00.000000', '2022-05-14 00:00:00.000000', '军事爱好者协会', '梅江区', null, null);
INSERT INTO `activity` VALUES ('12', '军事爱好者协会', '<p>军事爱好者协会招新啦，欢迎大家加入</p>', '1652022049680.jpg', null, '38', '2022-05-08 23:02:24', '招新对象', '2022-05-11 00:00:00.000000', '2022-05-14 00:00:00.000000', '军事爱好者协会', '本部', null, null);
INSERT INTO `activity` VALUES ('13', '军事爱好者协会欢迎你', '<p>军事爱好者协会是一个大家庭，欢迎大家的加入</p>', '1652022272607.jpg', null, '38', '2022-05-08 23:06:07', '招新对象', '2022-05-11 00:00:00.000000', '2022-05-18 00:00:00.000000', '军事爱好者协会', '校本部', null, null);
INSERT INTO `activity` VALUES ('14', '军事爱好者活动', '<p>军事爱好者活动欢迎你的参加</p>', '1652057292455.jpg', null, '38', '2022-05-09 08:49:10', '活动对象', '2022-05-11 00:00:00.000000', '2022-05-19 00:00:00.000000', '军事爱好者协会', '校内', null, null);
INSERT INTO `activity` VALUES ('15', '123123', '<p>132123</p>', '1652244367435.jpg', null, '38', '2022-05-11 12:46:17', '活动对象', '2022-05-27 00:00:00.000000', '2022-05-31 00:00:00.000000', '3123', '3123', null, null);
INSERT INTO `activity` VALUES ('16', '1231', '<p>321</p>', '1652244509071.jpg', null, '38', '2022-05-11 12:48:37', '活动对象', '2022-05-25 00:00:00.000000', '2022-05-26 00:00:00.000000', '3123', '3123', null, null);
INSERT INTO `activity` VALUES ('17', '123', '<p>2313</p>', '1652244542767.jpg', null, '38', '2022-05-11 12:49:11', '招新对象', '2022-05-24 00:00:00.000000', '2022-05-26 00:00:00.000000', '3123', '312', null, null);
INSERT INTO `activity` VALUES ('18', '活动', '<p>312312</p>', '1652322124772.jpg', null, '38', '2022-05-12 10:22:22', '活动对象', '2022-05-12 00:00:00.000000', '2022-05-13 00:00:00.000000', '123123', '12313', null, null);
INSERT INTO `activity` VALUES ('19', '测试专用活动', '<p>测试专用活动测试专用活动</p>', '1652780632383.jpg', '2022-05-17 21:05:22', '43', '2022-05-17 17:45:38', '活动对象', '2022-05-15 00:00:00.000000', '2022-06-23 00:00:00.000000', '测试专用活动', '测试专用活动', null, '更改活动对象');
INSERT INTO `activity` VALUES ('20', '测试社团招新活动', '<p>测试社团招新活动</p>', '1652789975973.jpg', null, '43', '2022-05-17 20:29:54', '招新对象', '2022-05-16 00:00:00.000000', '2022-06-30 00:00:00.000000', '测试社团招新活动', '测试社团招新活动', null, '测试社团招新活动');
INSERT INTO `activity` VALUES ('21', '测试社团招新啦', '<p>测试社团招新生，欢迎大家加入</p>', '1652892706021.jpg', null, '44', '2022-05-19 00:52:33', '招新对象', '2022-05-20 00:00:00.000000', '2022-05-21 00:00:00.000000', '测试社团', '测试社团的学校', null, '大一新生');
INSERT INTO `activity` VALUES ('22', '测试社团的活动', '<p>测试社团的社团活动地点</p>', '1652892999007.jpg', null, '44', '2022-05-19 00:57:30', '活动对象', '2022-05-20 00:00:00.000000', '2022-06-28 00:00:00.000000', '测试社团', '测试社团的活动地址', null, '全体在校生');
INSERT INTO `activity` VALUES ('23', '测试社团活动1名称', '<p>测试社团活动的内容11</p>', '1652893117218.jpg', null, '44', '2022-05-19 00:59:36', '活动对象', '2022-05-21 00:00:00.000000', '2022-05-24 00:00:00.000000', '测试社团活动举办方', '测试社团活动地点1', null, '全体在校非毕业班学生');
INSERT INTO `activity` VALUES ('24', '测试社团的社团活动名称', '<p>测试社团的·社团活动内容</p>', '1652945946314.jpg', null, '44', '2022-05-19 15:40:22', '活动对象', '2022-05-20 00:00:00.000000', '2022-05-27 00:00:00.000000', '测试社团的社团活动举办方', '测试社团的社团活动地点', null, '全体在校生');
INSERT INTO `activity` VALUES ('25', '军事爱好者社团活动名称', '<p>军事爱好者协会社团内容</p>', '1652946044069.jpg', null, '38', '2022-05-19 15:42:28', '活动对象', '2022-05-20 00:00:00.000000', '2022-05-27 00:00:00.000000', '军事爱好者协会的举办方', '军事爱好者协会的社团活动的地点', null, '大一、大二');
INSERT INTO `activity` VALUES ('26', '测试社团招新啦111', '<p>测试社团的社团活动内容</p>', '1652946223487.jpg', null, '44', '2022-05-19 16:09:27', '招新对象', '2022-05-20 00:00:00.000000', '2022-05-27 00:00:00.000000', '测试社团招新举办方', '测试社团招新的活动地点', null, '大一新生');
INSERT INTO `activity` VALUES ('27', '测试社团活动招新啦', '<p>去欢迎加入测试社团</p>', '1652948401922.jpg', null, '44', '2022-05-19 16:24:25', '招新对象', '2022-05-20 00:00:00.000000', '2022-05-26 00:00:00.000000', '测试社团', '测试社团的社团活动地点', null, '大一');
INSERT INTO `activity` VALUES ('28', 'pop音乐协会招新啦', '<p>欢迎大一新生加入pop音乐协会，让你的歌声传遍校园</p>', '1652977548900.jpg', null, '47', '2022-05-20 00:28:13', '招新对象', '2022-05-26 00:00:00.000000', '2022-05-27 00:00:00.000000', 'pop音乐协会', '活活艺术楼', null, '大一新生');
INSERT INTO `activity` VALUES ('29', 'pop音乐协会招新啦', '<p>欢迎新生们加入pop音乐协会</p>', '1652978506245.jpg', null, '47', '2022-05-20 00:43:26', '招新对象', '2022-05-21 00:00:00.000000', '2022-05-24 00:00:00.000000', 'pop音乐协会', '世纪广场', null, '大一新生');
INSERT INTO `activity` VALUES ('30', 'pop音乐协会元宵演唱会', '<p>元宵节当天，pop音乐协会组织歌唱演出</p>', '1652978836688.jpg', null, '47', '2022-05-20 00:50:02', '活动对象', '2022-05-21 00:00:00.000000', '2022-05-27 00:00:00.000000', 'pop音乐协会、音乐与舞蹈学院团学', '世纪广场', null, '全体在校生');
INSERT INTO `activity` VALUES ('31', 'pop音乐协会元宵演唱会', '<p>1111</p>', '1652979239590.jpg', null, '47', '2022-05-20 00:54:59', '活动对象', '2022-05-20 00:00:00.000000', '2022-05-21 00:00:00.000000', 'pop音乐协会', '德龙会堂', null, '全体非毕业生');
INSERT INTO `activity` VALUES ('32', '测试pop音乐活动', '<p>测试pop音乐活动</p>', '1653009408619.jpg', null, '47', '2022-05-20 09:17:29', '活动对象', '2022-05-19 02:02:02.000000', '2022-06-30 01:01:01.000000', '测试pop音乐活动', '测试pop音乐活动', null, '测试pop音乐活动');
INSERT INTO `activity` VALUES ('33', 'pop音乐协会招新啦', '<p>pop音乐协会招新是每年秋季展开的为协会补充新血液活动，加入pop音乐协会，让你的歌声传遍校园每个角落。</p>', '1653060471420.jpg', null, '47', '2022-05-20 23:44:04', '招新对象', '2022-05-20 00:00:00.000000', '2022-05-23 00:00:00.000000', 'pop音乐协会', '世纪广场', null, '全体在校生');
INSERT INTO `activity` VALUES ('34', 'pop音乐协会骑行活动', '<p>pop音乐协会骑行活动是由pop音乐协会和嘉应学院学生会共同举办，是绕梅江区骑行的公益活动，由此提倡大家锻炼身体</p>', '1653061529992.jpg', null, '47', '2022-05-20 23:49:17', '活动对象', '2022-05-23 09:31:00.000000', '2022-05-26 16:30:00.000000', 'pop音乐协会、嘉应学院学生会', '梅江区', null, '全体在校非毕业生');
INSERT INTO `activity` VALUES ('35', '1111', '<p>1111</p>', '1653093966708.jpg', null, '47', '2022-05-21 08:46:24', '活动对象', '2022-05-27 06:00:00.000000', '2022-05-28 10:00:00.000000', '111', '1111', null, '111');
INSERT INTO `activity` VALUES ('36', '篮球比赛', '<p>111</p>', '1653094064218.jpg', null, '47', '2022-05-21 08:48:08', '活动对象', '2022-05-20 06:00:00.000000', '2022-05-27 06:00:00.000000', '111', '111', null, '111');

-- ----------------------------
-- Table structure for affiche_info
-- ----------------------------
DROP TABLE IF EXISTS `affiche_info`;
CREATE TABLE `affiche_info` (
  `afficheId` int(11) NOT NULL AUTO_INCREMENT,
  `afficheTheme` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '公告主题',
  `afficheTime` datetime DEFAULT NULL COMMENT '时间',
  `affichePlace` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '地点',
  `afficheContent` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '内容',
  `announceTime` datetime DEFAULT NULL COMMENT '发布时间',
  `icon` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '图片',
  `organization_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`afficheId`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=28 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of affiche_info
-- ----------------------------
INSERT INTO `affiche_info` VALUES ('13', '关于五一放假的通知', '2022-05-07 23:08:30', '本系统内', '<p>五一假期来临，五一期间暂停使用学生社团管理系统，如有不便请见谅</p>', null, '1651936088572.jpg', '0');
INSERT INTO `affiche_info` VALUES ('16', '关于军事爱好者协会迎新生晚会的通知', '2022-05-10 00:00:00', '大会堂', '<p>将于周六晚上七点举行军事爱好者协会迎新晚会，请大家互相告知</p>', null, '1652022699037.jpg', '38');
INSERT INTO `affiche_info` VALUES ('17', '关于系统公告的通知', '2022-05-09 00:41:54', '校内', '<p>系统公告测试</p>', null, '1652028077220.jpg', '0');
INSERT INTO `affiche_info` VALUES ('18', '关于乒乓球协会公告的通知', '2022-05-09 00:44:58', '学校', '<p>这是乒乓球协会公告测试</p>', null, '1652028266147.jpg', '34');
INSERT INTO `affiche_info` VALUES ('19', '3213', '2022-05-11 12:48:50', '123', '<p>231</p>', null, '1652244526926.jpg', '38');
INSERT INTO `affiche_info` VALUES ('20', '测试的系统公告主题', '2022-05-19 00:18:39', '测试的系统公告地点', '<p>测试的系统公告内容</p>', null, '1652890697182.jpg', '0');
INSERT INTO `affiche_info` VALUES ('21', '测试社团的社团公告', '2022-05-19 01:04:24', '测试社团的社团公告的地点', '<p>测试社团的社团公告的内容</p>', null, '1652893433152.jpeg', '44');
INSERT INTO `affiche_info` VALUES ('22', 'pop音乐协会社团公告', '2022-05-20 00:55:37', '系统内', '<p>pop音乐协会的相关内容</p>', null, '1652979316726.jpg', '47');
INSERT INTO `affiche_info` VALUES ('23', '关于学习党史的通知', '2022-05-20 19:42:34', '系统内', '<p>学习党史能够加深对党的认识，重温党史，不忘初心，牢记使命。</p>', null, '1653046915410.jpg', '0');
INSERT INTO `affiche_info` VALUES ('26', '关于pop音乐协会在本周末举行演奏会的通知', '2022-05-19 23:51:12', '活活艺术楼', '<p>演奏会参与团队有小鱼儿团队、海绵宝宝团队、派大星团队、小虾米团队，将演奏内容结合了中西风格，欢迎大家的参加</p>', null, '1653061781622.jpg', '47');
INSERT INTO `affiche_info` VALUES ('27', '学党史、悟思想、强信念', '2022-05-21 00:40:03', '系统', '<p>学习党史，提升思想觉悟，坚守理想信念</p>', null, '1653064735010.jpg', '0');

-- ----------------------------
-- Table structure for class
-- ----------------------------
DROP TABLE IF EXISTS `class`;
CREATE TABLE `class` (
  `class_id` int(11) NOT NULL AUTO_INCREMENT,
  `addtime` datetime DEFAULT CURRENT_TIMESTAMP,
  `faculty` int(11) DEFAULT NULL,
  `major` int(11) DEFAULT NULL,
  `class_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  PRIMARY KEY (`class_id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=29 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of class
-- ----------------------------
INSERT INTO `class` VALUES ('3', '2022-04-23 17:54:25', '5', '2', '1901');
INSERT INTO `class` VALUES ('4', '2022-05-07 18:12:50', '2', '3', '1801');
INSERT INTO `class` VALUES ('5', '2022-05-07 18:13:04', '2', '3', '1802');
INSERT INTO `class` VALUES ('6', '2022-05-07 18:13:43', '2', '4', '1804');
INSERT INTO `class` VALUES ('7', '2022-05-07 18:14:05', '2', '4', '1803');
INSERT INTO `class` VALUES ('8', '2022-05-07 18:14:21', '2', '5', '1805');
INSERT INTO `class` VALUES ('9', '2022-05-07 18:15:00', '2', '5', '1806');
INSERT INTO `class` VALUES ('10', '2022-05-07 22:49:12', '3', '6', '1809');
INSERT INTO `class` VALUES ('11', '2022-05-07 22:53:27', '8', '12', '2021');
INSERT INTO `class` VALUES ('12', '2022-05-08 09:55:26', '2', '13', '1807');
INSERT INTO `class` VALUES ('13', '2022-05-08 09:56:37', '2', '13', '1808');
INSERT INTO `class` VALUES ('14', '2022-05-08 19:07:06', '8', '12', 'fdsfds');
INSERT INTO `class` VALUES ('15', '2022-05-08 20:02:02', '2', '3', '2002');
INSERT INTO `class` VALUES ('16', '2022-05-09 10:06:14', '2', '5', '2005');
INSERT INTO `class` VALUES ('17', '2022-05-09 11:38:22', '2', '3', '2001');
INSERT INTO `class` VALUES ('18', '2022-05-19 00:28:25', '11', '15', '1803');
INSERT INTO `class` VALUES ('19', '2022-05-19 14:55:48', '12', '17', '1801');
INSERT INTO `class` VALUES ('20', '2022-05-19 14:55:58', '12', '17', '1802');
INSERT INTO `class` VALUES ('21', '2022-05-19 14:56:10', '12', '18', '1803');
INSERT INTO `class` VALUES ('22', '2022-05-19 14:56:25', '12', '18', '1804');
INSERT INTO `class` VALUES ('23', '2022-05-21 00:42:54', '4', '9', '1803');
INSERT INTO `class` VALUES ('24', '2022-05-21 00:43:03', '4', '9', '1804');
INSERT INTO `class` VALUES ('25', '2022-05-21 00:43:12', '6', '10', '1801');
INSERT INTO `class` VALUES ('26', '2022-05-21 00:43:21', '6', '10', '1802');
INSERT INTO `class` VALUES ('27', '2022-05-21 00:44:02', '10', '19', '1801');
INSERT INTO `class` VALUES ('28', '2022-05-21 00:44:12', '10', '19', '1802');

-- ----------------------------
-- Table structure for club_type
-- ----------------------------
DROP TABLE IF EXISTS `club_type`;
CREATE TABLE `club_type` (
  `type_id` int(11) NOT NULL AUTO_INCREMENT,
  `type_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  PRIMARY KEY (`type_id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of club_type
-- ----------------------------
INSERT INTO `club_type` VALUES ('1', '文化体育型');
INSERT INTO `club_type` VALUES ('2', '艺术表演型');
INSERT INTO `club_type` VALUES ('3', '应用实践型');
INSERT INTO `club_type` VALUES ('4', '兴趣爱好型');
INSERT INTO `club_type` VALUES ('5', '爱心公益');
INSERT INTO `club_type` VALUES ('6', '志愿服务型');

-- ----------------------------
-- Table structure for comment
-- ----------------------------
DROP TABLE IF EXISTS `comment`;
CREATE TABLE `comment` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '评论id',
  `update_time` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `create_time` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `content` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '内容',
  `avatar` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '发布用户头像',
  `nickname` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '用户昵称',
  `user_id` bigint(11) DEFAULT NULL COMMENT '用户id',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=25 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of comment
-- ----------------------------
INSERT INTO `comment` VALUES ('11', null, '2022-05-07 19:42:04', '<p>今天天气不错</p>', '1651923671225.jpg', '蓝天白云', '61');
INSERT INTO `comment` VALUES ('13', null, '2022-05-07 19:45:41', '<p>怎么样进计算机协会呢</p>', '1651923671225.jpg', '蓝天白云', '61');
INSERT INTO `comment` VALUES ('14', null, '2022-05-08 09:40:57', '<p>好困呀</p>', '1651935826342.jpg', '蓝天白云', '61');
INSERT INTO `comment` VALUES ('16', null, '2022-05-08 16:31:14', '<p>13213</p>', '1651935826342.jpg', '蓝天白云', '61');
INSERT INTO `comment` VALUES ('18', null, '2022-05-08 16:32:57', '<p>今天是个好日子</p>', '1650174247050.jpg', '123', '53');
INSERT INTO `comment` VALUES ('19', null, '2022-05-08 19:59:56', '<p>213123</p>', '1652009463914.jpg', '测试昵称', '66');
INSERT INTO `comment` VALUES ('20', null, '2022-05-18 23:55:28', '<p>123</p>', '1652889312272.jpg', '测试用户昵称', '118');
INSERT INTO `comment` VALUES ('23', null, '2022-05-19 15:16:24', '<p>早起的鸟儿有虫吃</p>', '1652944445683.jpg', '施主您好呀', '119');
INSERT INTO `comment` VALUES ('24', null, '2022-05-21 00:42:09', '<p>学习新思想，加强理想信念</p>', '1653062963643.jpg', '遥望星空', '125');

-- ----------------------------
-- Table structure for department
-- ----------------------------
DROP TABLE IF EXISTS `department`;
CREATE TABLE `department` (
  `department_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `department_name` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '院系名称',
  `create_time` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`department_id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of department
-- ----------------------------
INSERT INTO `department` VALUES ('2', '计算机学院', '2022-04-21 12:59:06');
INSERT INTO `department` VALUES ('3', '化学与环境学院', '2022-04-21 13:59:06');
INSERT INTO `department` VALUES ('4', '土木工程学院', '2022-04-21 14:59:06');
INSERT INTO `department` VALUES ('5', '美术学院', '2022-04-21 15:59:06');
INSERT INTO `department` VALUES ('6', '文学院', '2022-04-21 16:59:06');
INSERT INTO `department` VALUES ('8', '经济与管理学院', '2022-05-07 22:51:23');
INSERT INTO `department` VALUES ('10', '体育学院', '2022-05-09 11:38:55');
INSERT INTO `department` VALUES ('11', '数学学院', '2022-05-19 00:25:04');
INSERT INTO `department` VALUES ('12', '音乐与舞蹈学院', '2022-05-19 14:53:53');

-- ----------------------------
-- Table structure for major
-- ----------------------------
DROP TABLE IF EXISTS `major`;
CREATE TABLE `major` (
  `major_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `major_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '专业名称',
  `create_time` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `faculty` int(11) DEFAULT NULL COMMENT '院系 id',
  PRIMARY KEY (`major_id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=20 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of major
-- ----------------------------
INSERT INTO `major` VALUES ('2', '美术师范', '2022-04-22 00:59:06', '5');
INSERT INTO `major` VALUES ('3', '计算机科学与技术', '2022-04-22 01:59:06', '2');
INSERT INTO `major` VALUES ('4', '软件工程', '2022-04-22 02:59:06', '2');
INSERT INTO `major` VALUES ('5', '网络工程', '2022-05-07 18:11:12', '2');
INSERT INTO `major` VALUES ('6', '化学师范', '2022-05-07 18:11:21', '3');
INSERT INTO `major` VALUES ('7', '应用化学', '2022-05-07 18:11:31', '3');
INSERT INTO `major` VALUES ('8', '环境工程', '2022-05-07 18:11:51', '3');
INSERT INTO `major` VALUES ('9', '土木工程专业', '2022-05-07 18:12:18', '4');
INSERT INTO `major` VALUES ('10', '心理学', '2022-05-07 18:12:27', '6');
INSERT INTO `major` VALUES ('11', '环境工程', '2022-05-07 20:01:06', '3');
INSERT INTO `major` VALUES ('12', '财务管理', '2022-05-07 22:52:21', '8');
INSERT INTO `major` VALUES ('13', '物联网', '2022-05-08 09:54:06', '2');
INSERT INTO `major` VALUES ('15', '数学师范', '2022-05-19 00:27:32', '11');
INSERT INTO `major` VALUES ('16', '应用统计', '2022-05-19 00:28:13', '11');
INSERT INTO `major` VALUES ('17', '音乐师范', '2022-05-19 14:54:29', '12');
INSERT INTO `major` VALUES ('18', '舞蹈师范', '2022-05-19 14:54:46', '12');
INSERT INTO `major` VALUES ('19', '运动训练', '2022-05-21 00:43:41', '10');

-- ----------------------------
-- Table structure for member_info
-- ----------------------------
DROP TABLE IF EXISTS `member_info`;
CREATE TABLE `member_info` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `memberId` int(11) NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `organization_id` int(11) DEFAULT NULL COMMENT '社团 id',
  `joinTime` datetime DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=36 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of member_info
-- ----------------------------
INSERT INTO `member_info` VALUES ('2', '3', '54', '20', null);
INSERT INTO `member_info` VALUES ('3', '6', '54', '20', null);
INSERT INTO `member_info` VALUES ('13', '8', '61', '23', '2022-05-08 19:00:28');
INSERT INTO `member_info` VALUES ('14', '7', '66', '24', '2022-05-08 19:32:54');
INSERT INTO `member_info` VALUES ('15', '7', '68', '38', '2022-05-08 22:41:31');
INSERT INTO `member_info` VALUES ('16', '7', '69', '38', '2022-05-10 22:58:04');
INSERT INTO `member_info` VALUES ('17', '3', '111', '38', '2022-05-12 09:35:29');
INSERT INTO `member_info` VALUES ('18', '7', '111', '38', '2022-05-12 09:57:14');
INSERT INTO `member_info` VALUES ('19', '5', '68', '38', '2022-05-12 09:57:37');
INSERT INTO `member_info` VALUES ('20', '7', '111', '38', '2022-05-12 09:59:10');
INSERT INTO `member_info` VALUES ('25', '4', '114', '43', '2022-05-17 18:36:20');
INSERT INTO `member_info` VALUES ('26', '5', '114', '43', '2022-05-17 18:37:15');
INSERT INTO `member_info` VALUES ('27', '7', '111', '44', '2022-05-19 01:13:02');
INSERT INTO `member_info` VALUES ('29', '6', '119', '38', '2022-05-19 15:36:42');
INSERT INTO `member_info` VALUES ('30', '7', '121', '38', '2022-05-19 17:36:18');
INSERT INTO `member_info` VALUES ('33', '6', '123', '47', '2022-05-20 23:25:37');
INSERT INTO `member_info` VALUES ('34', '7', '124', '47', '2022-05-21 00:06:58');

-- ----------------------------
-- Table structure for organization
-- ----------------------------
DROP TABLE IF EXISTS `organization`;
CREATE TABLE `organization` (
  `organization_id` int(11) NOT NULL AUTO_INCREMENT,
  `organization_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '社团名称',
  `department_id` int(11) NOT NULL COMMENT '所属院系',
  `icon` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '社团旗帜',
  `people_num` int(11) DEFAULT '0' COMMENT '社团成员人数',
  `introduction` varchar(5000) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '介绍',
  `create_time` timestamp NULL DEFAULT CURRENT_TIMESTAMP COMMENT '成立时间',
  `update_time` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `user_id` int(11) DEFAULT NULL COMMENT '社团管理员id/会长',
  `tenet` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '社团宗旨',
  `synopsis` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '社团简介',
  `brandName` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '品牌活动名称',
  `brandContent` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '品牌活动内容',
  `associType` int(11) DEFAULT NULL COMMENT '社团类型',
  `vip_num` int(11) DEFAULT NULL COMMENT '社团会员人数',
  `status` int(11) DEFAULT '0' COMMENT '0: 审核中 1: 审核通过 2: 审核不通过',
  PRIMARY KEY (`organization_id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=49 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of organization
-- ----------------------------
INSERT INTO `organization` VALUES ('18', '英语协会', '2', '1637588089012.jpg', '0', '<p>帮助同学提高英语口语、欢迎大家加入。<img src=\"http://localhost:21505/images/1651926261024.jpg\"></p>', '2021-11-22 21:35:08', '2022-05-07 20:24:23', '61', '这是英语协会的社团宗旨', null, '英语角辩论', '英语角辩论是通过英语辩论的形式提高英语交流', '2', null, '0');
INSERT INTO `organization` VALUES ('19', '中国围棋协会', '5', '1637588716866.webp', '1', '<p>中国围棋协会是全国唯一的围棋认证机构</p>', '2021-11-22 21:45:33', '2022-05-20 19:30:51', '53', '这是中国围棋协会的社团宗旨', null, '围棋大赛杯', '中国围棋大赛是全国性比赛', '1', null, '1');
INSERT INTO `organization` VALUES ('20', '酷漫舍', '2', '1651970747908.jpg', '0', '<p>这是酷漫舍的社团简介</p>', '2022-04-16 18:05:27', '2022-05-08 08:45:53', '51', '这是酷漫舍的社团宗旨', null, '酷漫舍动漫大赛', '喜欢动漫的同学可以参加', '2', null, '1');
INSERT INTO `organization` VALUES ('23', '乒乓球协会', '3', '1651970658799.jpg', '0', '<p>这是乒乓球球协会的社团简介</p>', '2022-04-16 23:43:08', '2022-05-08 08:44:21', '51', '这是乒乓球协会的社团宗旨', null, '乒乓球混双比赛', '乒乓球混双比赛内容是团体活动', '1', null, '1');
INSERT INTO `organization` VALUES ('24', '篮球协会', '2', '1651970681337.jpg', '0', '<p>这是篮球协会的社团简介</p>', '2022-04-17 16:22:16', '2022-05-08 08:45:35', '55', '这是篮球协会的社团宗旨', null, '篮球新生杯', '篮球新生杯是新生参加的活动', '1', null, '1');
INSERT INTO `organization` VALUES ('27', '毽球协会', '2', '1651931406932.jpg', '0', '毽球协会社团简介', '2022-05-07 21:50:47', null, '62', '毽球协会宗旨', null, '毽球协会品牌活动名称', '毽球协会品牌活动内容', '1', null, '0');
INSERT INTO `organization` VALUES ('32', '123', '2', '1651976258245.jpg', '0', '<p>3123</p>', '2022-05-08 10:17:44', '2022-05-19 00:02:48', '61', '3123', null, '3123', '3123', '1', null, '1');
INSERT INTO `organization` VALUES ('34', '乒乓球协会', '2', '1651987370083.jpg', '0', '<p>乒乓球混双比赛每组二对二对战</p>', '2022-05-08 13:22:57', '2022-05-08 21:02:37', '61', '强身健体乒乓球运动', null, '乒乓球混双比赛', '乒乓球混双比赛', '1', null, '1');
INSERT INTO `organization` VALUES ('38', '军事爱好者协会', '2', '1652015334943.jpg', '0', '军事爱好者协会是五湖四海热爱军事的朋友的基地', '2022-05-08 21:12:26', '2022-05-08 21:30:41', '67', '热爱军事，爱国爱民', null, '军事知识大赛', '这是关于军事的知识大赛，每年都有许多人参加', '4', null, '1');
INSERT INTO `organization` VALUES ('39', '爱心公益发明', '5', '1652194528106.jpg', '0', '31313', '2022-05-10 22:56:20', '2022-05-14 15:29:05', '69', '爱心公益发明的社团宗旨', null, '爱心收集活动', '爱心收集活动内容', '5', null, '1');
INSERT INTO `organization` VALUES ('42', '测试社团', '2', '1652515434127.jpg', '0', '测试社团测试社团测试社团', '2022-05-14 16:08:23', '2022-05-19 00:24:04', '113', '测试社团测试社团测试社团', null, '测试社团', '测试社团', '2', null, '2');
INSERT INTO `organization` VALUES ('43', '武术协会', '2', '1653046432402.jpg', '0', '<p>武术协会率属于计算机学院单位，是校级社团，以武会友，吸引众多爱好武术的朋友参加，也欢迎各位同学的加入</p>', '2022-05-14 17:21:41', '2022-05-20 19:40:02', '111', '以武会友', null, '武术比拼大赛', '武术比拼大赛是由武术协会每年十月份举办的大型活动', '1', null, '1');
INSERT INTO `organization` VALUES ('44', '测试社团名称', '2', '1652888934273.jpg', '0', '<p>测试社团的社团简介</p>', '2022-05-18 23:50:05', '2022-05-18 23:50:51', '68', '测试社团宗旨', null, '测试社团的品牌活动名称', '测试社团的品牌活动内容', '2', null, '1');
INSERT INTO `organization` VALUES ('45', '测试用户申请创建的社团', '6', '1652892495563.jpeg', '0', '测试用户申请创建的社团的社团简介', '2022-05-19 00:49:52', '2022-05-19 01:58:25', '118', '测试用户申请创建的社团的社团宗旨', null, '测试用户申请创建的社团的品牌活动名称', '测试用户申请创建的社团的品牌活动内容', '5', null, '1');
INSERT INTO `organization` VALUES ('47', 'pop音乐协会', '12', '1652975650548.jpg', '0', 'pop音乐协会属于音乐与舞蹈学院，欢迎大家加入', '2022-05-19 23:58:40', '2022-05-20 00:07:56', '121', 'pop音乐协会宗旨是让歌声传遍校园每个角落', null, '校园歌声大赛', '校园歌声大赛是音乐与舞蹈学院举办的歌曲大赛', '2', null, '1');
INSERT INTO `organization` VALUES ('48', '爱心志愿协会', '11', '1653059703141.jpg', '0', '爱心志愿协会率属于数学学院，是一个校级社团，主要是为贫困地区孩子捐献爱心，从事志愿相关活动内容。', '2022-05-20 23:20:16', '2022-05-20 23:21:48', '123', '奉献爱心，收获幸福', null, '校园爱心捐赠大会', '校园爱心捐助大会主要是征集学生们不用的衣物、书籍，由我们协会组织捐献给贫困地区的孩子', '6', null, '1');

-- ----------------------------
-- Table structure for org_info
-- ----------------------------
DROP TABLE IF EXISTS `org_info`;
CREATE TABLE `org_info` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL,
  `organization_id` int(11) DEFAULT NULL COMMENT '社团 id',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=56 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of org_info
-- ----------------------------
INSERT INTO `org_info` VALUES ('1', '53', '23');
INSERT INTO `org_info` VALUES ('3', '53', '24');
INSERT INTO `org_info` VALUES ('4', '51', '20');
INSERT INTO `org_info` VALUES ('22', '61', '34');
INSERT INTO `org_info` VALUES ('27', '67', '38');
INSERT INTO `org_info` VALUES ('30', '68', '38');
INSERT INTO `org_info` VALUES ('32', '69', '38');
INSERT INTO `org_info` VALUES ('33', '69', '39');
INSERT INTO `org_info` VALUES ('40', '118', '45');
INSERT INTO `org_info` VALUES ('41', '61', '42');
INSERT INTO `org_info` VALUES ('42', '118', '43');
INSERT INTO `org_info` VALUES ('44', '119', '24');
INSERT INTO `org_info` VALUES ('45', '119', '44');
INSERT INTO `org_info` VALUES ('46', '118', '38');
INSERT INTO `org_info` VALUES ('48', '120', '44');
INSERT INTO `org_info` VALUES ('52', '121', '47');
INSERT INTO `org_info` VALUES ('54', '123', '48');
INSERT INTO `org_info` VALUES ('55', '124', '47');

-- ----------------------------
-- Table structure for quit_association
-- ----------------------------
DROP TABLE IF EXISTS `quit_association`;
CREATE TABLE `quit_association` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL,
  `reason` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `applyTime` datetime DEFAULT CURRENT_TIMESTAMP,
  `organization_id` int(11) DEFAULT NULL,
  `status` int(11) DEFAULT '0',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of quit_association
-- ----------------------------
INSERT INTO `quit_association` VALUES ('2', '57', null, '2022-04-25 22:44:27', '26', '1');
INSERT INTO `quit_association` VALUES ('3', '61', null, '2022-05-07 22:13:02', '26', '2');
INSERT INTO `quit_association` VALUES ('4', '61', null, '2022-05-08 17:06:24', '34', '0');
INSERT INTO `quit_association` VALUES ('5', '67', null, '2022-05-10 10:17:38', '34', '1');
INSERT INTO `quit_association` VALUES ('6', '111', null, '2022-05-12 09:35:38', '38', '2');
INSERT INTO `quit_association` VALUES ('7', '114', null, '2022-05-17 16:13:16', '43', '0');
INSERT INTO `quit_association` VALUES ('8', '115', '想试一下退团', '2022-05-18 00:07:52', '43', '2');
INSERT INTO `quit_association` VALUES ('9', '118', '我想退出测试社团', '2022-05-19 01:14:00', '44', '1');
INSERT INTO `quit_association` VALUES ('10', '120', '退出', '2022-05-19 16:44:29', '38', '1');
INSERT INTO `quit_association` VALUES ('11', '122', '我想退出社团', '2022-05-20 01:04:13', '47', '1');

-- ----------------------------
-- Table structure for sector_info
-- ----------------------------
DROP TABLE IF EXISTS `sector_info`;
CREATE TABLE `sector_info` (
  `sec_id` int(11) NOT NULL AUTO_INCREMENT,
  `organization_id` int(11) DEFAULT NULL COMMENT '社团 id',
  `sector_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '部门名称',
  `sector_duty` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '部门职责',
  PRIMARY KEY (`sec_id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of sector_info
-- ----------------------------
INSERT INTO `sector_info` VALUES ('4', '20', '12312', '124321');
INSERT INTO `sector_info` VALUES ('5', '0', '财务部', '管理钱财');
INSERT INTO `sector_info` VALUES ('8', '38', '外联部', '负责联谊，拉赞助');
INSERT INTO `sector_info` VALUES ('9', '38', '学习部', '收集资料，加综测');
INSERT INTO `sector_info` VALUES ('10', '38', '信息部', '对公众号进行管理，以及配合其他部门工作');
INSERT INTO `sector_info` VALUES ('11', '43', '天上部门', '负责天上行为');
INSERT INTO `sector_info` VALUES ('12', '44', '科技部', '主要是维护公众号');
INSERT INTO `sector_info` VALUES ('13', '38', '军械部', '管理军械物资');
INSERT INTO `sector_info` VALUES ('14', '47', '演艺部', '主要是进行各类唱歌演出');
INSERT INTO `sector_info` VALUES ('15', '47', '宣传部', '负责协会的宣传招新等内容');

-- ----------------------------
-- Table structure for stuposition
-- ----------------------------
DROP TABLE IF EXISTS `stuposition`;
CREATE TABLE `stuposition` (
  `posi_id` int(11) NOT NULL AUTO_INCREMENT,
  `addtime` datetime DEFAULT CURRENT_TIMESTAMP,
  `stu_position` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  PRIMARY KEY (`posi_id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of stuposition
-- ----------------------------
INSERT INTO `stuposition` VALUES ('3', '2022-04-10 20:06:56', '会长');
INSERT INTO `stuposition` VALUES ('4', '2022-04-23 16:22:31', '副会长');
INSERT INTO `stuposition` VALUES ('5', '2022-04-23 16:22:43', '部长');
INSERT INTO `stuposition` VALUES ('6', '2022-04-23 16:22:47', '副部长');
INSERT INTO `stuposition` VALUES ('7', '2022-04-23 16:22:52', '干事');
INSERT INTO `stuposition` VALUES ('8', '2022-05-07 22:57:00', '普通用户');

-- ----------------------------
-- Table structure for users
-- ----------------------------
DROP TABLE IF EXISTS `users`;
CREATE TABLE `users` (
  `user_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `username` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '用户名',
  `password` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '密码',
  `nickname` varchar(45) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '手机号',
  `phone` varchar(45) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `avatar` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT '1637597338099.jpg' COMMENT '头像',
  `sex` varchar(10) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '性别',
  `update_time` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `create_time` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `stuFaculty` int(11) DEFAULT NULL COMMENT '学院',
  `stuMajor` int(11) DEFAULT NULL COMMENT '专业',
  `stuClass` int(11) DEFAULT NULL COMMENT '班级',
  `stuId` bigint(11) NOT NULL COMMENT '学号',
  PRIMARY KEY (`user_id`,`username`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=127 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of users
-- ----------------------------
INSERT INTO `users` VALUES ('51', 'a92564', '03edd98d72d50c1eaa643e34d0566e3d', '蓝天白云', '11111111', '1651930044784.jpg', '女', '2022-05-19 18:24:51', '2022-04-06 23:07:27', '2', '4', '6', '0');
INSERT INTO `users` VALUES ('53', 'a123456', '03edd98d72d50c1eaa643e34d0566e3d', 'hello', '189766874', '1650174247050.jpg', '男', '2022-05-20 19:30:06', '2022-04-16 23:25:39', '5', '2', '3', '123456');
INSERT INTO `users` VALUES ('56', 'a92567', '03edd98d72d50c1eaa643e34d0566e3d', 'xxyy', '18319503654', '1650896367790.png', '男', '2022-05-20 19:29:31', '2022-04-25 22:18:13', '5', '2', '3', '0');
INSERT INTO `users` VALUES ('57', 'a92568', '03edd98d72d50c1eaa643e34d0566e3d', '3213213', '18813313333', '1650898350243.png', '男', '2022-05-19 14:18:02', '2022-04-25 22:23:50', '5', '2', '3', '0');
INSERT INTO `users` VALUES ('61', '李小埋', '51333f3428abe4c88d43a2f2c1198a19', '小埋爱吃瓜', '18311111111', '1651935826342.jpg', '女', '2022-05-08 21:38:24', '2022-05-07 19:38:16', '2', '4', '7', '88888888');
INSERT INTO `users` VALUES ('66', '测试22', '03edd98d72d50c1eaa643e34d0566e3d', '测试昵称', '12012542011', '1652009463914.jpg', '男', '2022-05-08 21:14:05', '2022-05-08 19:30:22', '2', '4', '6', '123456');
INSERT INTO `users` VALUES ('67', '李小测', '03edd98d72d50c1eaa643e34d0566e3d', '李小测昵称', '18813313333', '1652015231945.jpg', '男', '2022-05-11 20:52:08', '2022-05-08 21:05:24', '2', '3', '5', '123456');
INSERT INTO `users` VALUES ('68', '李小测2号', '03edd98d72d50c1eaa643e34d0566e3d', '李小测2号昵称', '18319999858', '1652017290866.jpg', '女', '2022-05-08 21:41:54', '2022-05-08 21:39:31', '3', '6', '10', '181011111');
INSERT INTO `users` VALUES ('69', '111', '03edd98d72d50c1eaa643e34d0566e3d', '21313', '13654545201', '1652193614833.jpg', '男', '2022-05-19 15:22:56', '2022-05-10 22:39:41', '2', '3', '4', '222');
INSERT INTO `users` VALUES ('111', '小李庄', '37756bd3cff2f04c82a0af2554a49b54', '小李庄昵称', '19999999999999', '1652274027881.jpg', '男', '2022-05-11 21:00:28', '2022-05-11 20:58:53', '3', '6', '10', '123123');
INSERT INTO `users` VALUES ('112', 'admin', '03edd98d72d50c1eaa643e34d0566e3d', 'admin', 'undefined', '1637597338099.jpg', '男', '2022-05-19 17:07:47', '2022-03-21 00:09:03', '5', '2', '3', '0');
INSERT INTO `users` VALUES ('114', 'YeTong', 'e9cef08c404a9ac3d1c15a4b49cd7fa5', 'YeTong', '19318981231', '1652941119308.jpg', '男', '2022-05-19 14:18:40', '2022-05-14 17:49:08', '2', '4', '6', '10085');
INSERT INTO `users` VALUES ('115', 'Ceshi322', 'e9cef08c404a9ac3d1c15a4b49cd7fa5', 'Ceshi322', '13699999098', '1652944714216.jpg', '男', '2022-05-19 15:18:35', '2022-05-17 21:57:31', '2', '3', '4', '19999');
INSERT INTO `users` VALUES ('118', '测试用户姓名', '5fd414f5def965ff6ce6159131edd024', '测试用户昵称', '18810010000', '1652889312272.jpg', '男', '2022-05-19 01:55:27', '2022-05-18 23:53:15', '3', '6', '10', '111111111');
INSERT INTO `users` VALUES ('119', '施主', '03edd98d72d50c1eaa643e34d0566e3d', '施主您好呀', '18813313115', '1652944445683.jpg', '男', '2022-05-19 15:14:07', '2022-05-19 15:12:59', '8', '12', '11', '181000000');
INSERT INTO `users` VALUES ('120', '小白', '03edd98d72d50c1eaa643e34d0566e3d', '小白昵称', '187164857654', '1652949667143.jpg', '女', '2022-05-19 16:41:07', '2022-05-19 16:40:09', '5', '2', '3', '222222');
INSERT INTO `users` VALUES ('121', '小黑', '03edd98d72d50c1eaa643e34d0566e3d', '小黑昵称', '15466446664', '1652952904081.jpg', '男', '2022-05-19 17:35:05', '2022-05-19 17:34:03', '12', '17', '19', '4442222');
INSERT INTO `users` VALUES ('122', '李白', '03edd98d72d50c1eaa643e34d0566e3d', '诗人李白', '18319588456', '1652976899179.jpg', '男', '2022-05-20 00:15:00', '2022-05-20 00:11:12', '12', '18', '22', '888888883');
INSERT INTO `users` VALUES ('123', '小青', '03edd98d72d50c1eaa643e34d0566e3d', '小青儿', '18319588665', '1653059655272.jpg', '女', '2022-05-20 23:14:17', '2022-05-20 23:11:54', '12', '17', '20', '123456789');
INSERT INTO `users` VALUES ('124', '小蓝', '03edd98d72d50c1eaa643e34d0566e3d', '小蓝儿', '188133333445', '1653062703292.jpg', '男', '2022-05-21 00:05:04', '2022-05-21 00:03:00', '2', '4', '7', '11223344');
INSERT INTO `users` VALUES ('125', '星空', '03edd98d72d50c1eaa643e34d0566e3d', '遥望星空', '18919852458', '1653062963643.jpg', '男', '2022-05-21 00:09:29', '2022-05-21 00:08:27', '5', '2', '3', '181030265');
INSERT INTO `users` VALUES ('126', '小龙', '03edd98d72d50c1eaa643e34d0566e3d', '小龙儿', '19981971997371', '1653092219946.jpg', '男', '2022-05-21 08:17:01', '2022-05-21 08:16:11', '2', '4', '7', '11221111111');

-- ----------------------------
-- Table structure for user_activity
-- ----------------------------
DROP TABLE IF EXISTS `user_activity`;
CREATE TABLE `user_activity` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'id',
  `uid` int(11) NOT NULL COMMENT '用户id',
  `aid` int(11) NOT NULL COMMENT '活动id',
  `create_time` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `status` int(11) DEFAULT '0' COMMENT '0未审核 1已通过 2已拒绝',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=26 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of user_activity
-- ----------------------------
INSERT INTO `user_activity` VALUES ('5', '61', '4', '2022-05-08 15:48:40', '0');
INSERT INTO `user_activity` VALUES ('6', '61', '2', '2022-05-08 15:57:44', '0');
INSERT INTO `user_activity` VALUES ('7', '61', '5', '2022-05-08 17:16:35', '0');
INSERT INTO `user_activity` VALUES ('8', '61', '7', '2022-05-08 17:19:26', '1');
INSERT INTO `user_activity` VALUES ('10', '66', '9', '2022-05-08 19:53:39', '1');
INSERT INTO `user_activity` VALUES ('11', '68', '11', '2022-05-08 23:23:30', '1');
INSERT INTO `user_activity` VALUES ('12', '111', '2', '2022-05-11 21:08:04', '0');
INSERT INTO `user_activity` VALUES ('13', '111', '11', '2022-05-12 09:47:26', '0');
INSERT INTO `user_activity` VALUES ('14', '111', '18', '2022-05-12 11:08:29', '0');
INSERT INTO `user_activity` VALUES ('17', '114', '19', '2022-05-17 17:45:49', '1');
INSERT INTO `user_activity` VALUES ('18', '115', '19', '2022-05-17 23:29:01', '0');
INSERT INTO `user_activity` VALUES ('19', '118', '23', '2022-05-19 00:59:56', '1');
INSERT INTO `user_activity` VALUES ('20', '118', '7', '2022-05-19 01:18:35', '2');
INSERT INTO `user_activity` VALUES ('21', '122', '31', '2022-05-20 00:57:55', '1');
INSERT INTO `user_activity` VALUES ('22', '122', '32', '2022-05-20 19:41:08', '0');
INSERT INTO `user_activity` VALUES ('23', '123', '32', '2022-05-20 23:26:29', '1');
INSERT INTO `user_activity` VALUES ('24', '126', '35', '2022-05-21 08:46:45', '1');
INSERT INTO `user_activity` VALUES ('25', '126', '36', '2022-05-21 08:48:40', '0');

-- ----------------------------
-- Table structure for user_department
-- ----------------------------
DROP TABLE IF EXISTS `user_department`;
CREATE TABLE `user_department` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `uid` int(11) NOT NULL COMMENT '用户id',
  `oid` int(11) NOT NULL COMMENT '社团id',
  `status` int(11) DEFAULT '0' COMMENT '0未审核 1已通过 2已拒绝',
  `create_time` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `update_time` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `reason` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '申请理由',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=31 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of user_department
-- ----------------------------
INSERT INTO `user_department` VALUES ('1', '27', '11', '1', '2021-11-02 10:29:26', '2021-11-20 10:35:58', null);
INSERT INTO `user_department` VALUES ('2', '54', '24', '1', '2022-04-23 16:01:31', '2022-04-23 16:46:00', '123');
INSERT INTO `user_department` VALUES ('3', '54', '20', '1', '2022-04-23 18:31:07', '2022-04-23 18:44:34', '321313');
INSERT INTO `user_department` VALUES ('5', '57', '26', '1', '2022-04-25 22:35:52', '2022-04-25 22:36:04', 'woyaodanghuiyuan');
INSERT INTO `user_department` VALUES ('7', '61', '27', '0', '2022-05-08 17:15:20', null, '');
INSERT INTO `user_department` VALUES ('8', '61', '24', '1', '2022-05-08 17:34:41', '2022-05-08 19:21:49', '2222');
INSERT INTO `user_department` VALUES ('9', '61', '20', '0', '2022-05-08 19:18:49', null, 'ces');
INSERT INTO `user_department` VALUES ('10', '66', '24', '1', '2022-05-08 19:31:46', '2022-05-08 19:32:05', '测试22申请');
INSERT INTO `user_department` VALUES ('11', '66', '34', '0', '2022-05-08 20:58:54', null, '测试');
INSERT INTO `user_department` VALUES ('12', '68', '38', '1', '2022-05-08 21:45:10', '2022-05-08 22:16:00', '我想加入军事爱好者协会');
INSERT INTO `user_department` VALUES ('13', '67', '34', '1', '2022-05-10 10:16:31', '2022-05-10 10:17:18', '3123123');
INSERT INTO `user_department` VALUES ('14', '111', '38', '1', '2022-05-12 00:07:30', '2022-05-12 09:26:46', '1213');
INSERT INTO `user_department` VALUES ('16', '114', '43', '1', '2022-05-17 15:37:11', '2022-05-17 15:37:29', '测试一下申请原因');
INSERT INTO `user_department` VALUES ('17', '115', '43', '1', '2022-05-17 23:58:25', '2022-05-17 23:59:02', '想测试一下加入社团');
INSERT INTO `user_department` VALUES ('18', '118', '44', '1', '2022-05-19 00:44:01', '2022-05-19 00:44:35', '我想加入测试社团名称');
INSERT INTO `user_department` VALUES ('19', '118', '39', '0', '2022-05-19 01:19:41', null, '我有爱心');
INSERT INTO `user_department` VALUES ('20', '119', '38', '1', '2022-05-19 15:35:25', '2022-05-19 15:36:07', '我想加入军事爱好者协会');
INSERT INTO `user_department` VALUES ('21', '120', '24', '0', '2022-05-19 17:22:16', null, '123');
INSERT INTO `user_department` VALUES ('22', '121', '38', '1', '2022-05-19 17:35:32', '2022-05-19 17:35:40', '我想加入');
INSERT INTO `user_department` VALUES ('23', '122', '47', '1', '2022-05-20 00:15:29', '2022-05-20 00:18:28', '我想加入pop音乐协会');
INSERT INTO `user_department` VALUES ('24', '123', '47', '1', '2022-05-20 23:22:18', '2022-05-20 23:23:33', '我想加入pop音乐协会');
INSERT INTO `user_department` VALUES ('25', '124', '47', '1', '2022-05-21 00:05:40', '2022-05-21 00:06:10', '我想加入社团');
INSERT INTO `user_department` VALUES ('30', '125', '47', '1', '2022-05-21 07:33:31', '2022-05-21 07:34:33', '');

-- ----------------------------
-- Table structure for user_member
-- ----------------------------
DROP TABLE IF EXISTS `user_member`;
CREATE TABLE `user_member` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `memberId` int(11) NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `organization_id` int(11) DEFAULT NULL COMMENT '社团 id',
  `joinTime` datetime DEFAULT CURRENT_TIMESTAMP,
  `status` int(11) DEFAULT NULL,
  `reason` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=33 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of user_member
-- ----------------------------
INSERT INTO `user_member` VALUES ('1', '6', '54', '24', '2022-04-23 18:25:12', '0', '33');
INSERT INTO `user_member` VALUES ('2', '3', '54', '24', '2022-04-23 18:29:09', '0', '33');
INSERT INTO `user_member` VALUES ('3', '4', '54', '24', '2022-04-23 18:30:23', '0', '232');
INSERT INTO `user_member` VALUES ('4', '7', '54', '24', '2022-04-23 18:30:58', '0', '32133');
INSERT INTO `user_member` VALUES ('5', '3', '54', '20', '2022-04-23 19:00:49', '1', 'xx');
INSERT INTO `user_member` VALUES ('6', '6', '54', '20', '2022-04-23 19:19:36', '1', '我要当副部长');
INSERT INTO `user_member` VALUES ('10', '3', '55', '25', '2022-04-25 00:29:38', '1', '32131321');
INSERT INTO `user_member` VALUES ('11', '4', '56', '26', '2022-04-25 22:22:46', '1', 'fdsafsadfsadf');
INSERT INTO `user_member` VALUES ('12', '5', '57', '26', '2022-04-25 22:26:06', '1', 'fsdfdsfsdaf');
INSERT INTO `user_member` VALUES ('13', '4', '61', '26', '2022-05-07 22:11:52', '0', '我想加入');
INSERT INTO `user_member` VALUES ('15', '7', '61', '34', '2022-05-08 17:38:01', '0', '3123213');
INSERT INTO `user_member` VALUES ('16', '8', '61', '23', '2022-05-08 17:39:37', '1', '测试');
INSERT INTO `user_member` VALUES ('17', '7', '66', '24', '2022-05-08 19:32:36', '1', '测试申请职位');
INSERT INTO `user_member` VALUES ('18', '7', '68', '38', '2022-05-08 22:32:07', '1', '我想加入');
INSERT INTO `user_member` VALUES ('20', '3', '111', '38', '2022-05-12 09:27:01', '1', '12313');
INSERT INTO `user_member` VALUES ('21', '7', '111', '38', '2022-05-12 09:59:00', '1', '313123');
INSERT INTO `user_member` VALUES ('22', '3', '113', '43', '2022-05-14 17:42:09', '0', '');
INSERT INTO `user_member` VALUES ('24', '5', '114', '43', '2022-05-17 16:05:59', '1', '我又来测试部长申请');
INSERT INTO `user_member` VALUES ('25', '5', '118', '45', '2022-05-19 14:51:15', '0', '我想成为部长');
INSERT INTO `user_member` VALUES ('26', '6', '119', '38', '2022-05-19 15:36:34', '1', '我想成为副部长');
INSERT INTO `user_member` VALUES ('27', '7', '121', '38', '2022-05-19 17:36:04', '1', '我想成为干事');
INSERT INTO `user_member` VALUES ('28', '6', '122', '47', '2022-05-20 00:19:04', '1', '我想成为副部长');
INSERT INTO `user_member` VALUES ('29', '6', '123', '47', '2022-05-20 23:25:26', '1', '我想成为pop音乐协会的副部长');
INSERT INTO `user_member` VALUES ('30', '7', '124', '47', '2022-05-21 00:06:44', '1', '我想成为pop音乐协会干事');
INSERT INTO `user_member` VALUES ('32', '5', '125', '47', '2022-05-21 07:34:46', '2', '123123的司法文书');

-- ----------------------------
-- Table structure for vip_info
-- ----------------------------
DROP TABLE IF EXISTS `vip_info`;
CREATE TABLE `vip_info` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `organization_id` int(11) DEFAULT NULL,
  `joinTime` datetime DEFAULT CURRENT_TIMESTAMP,
  `user_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=27 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of vip_info
-- ----------------------------
INSERT INTO `vip_info` VALUES ('1', '24', null, '54');
INSERT INTO `vip_info` VALUES ('2', '20', '2022-04-23 18:44:34', '54');
INSERT INTO `vip_info` VALUES ('5', '24', '2022-05-08 19:21:49', '61');
INSERT INTO `vip_info` VALUES ('6', '24', '2022-05-08 19:32:05', '66');
INSERT INTO `vip_info` VALUES ('7', '38', '2022-05-08 22:16:00', '68');
INSERT INTO `vip_info` VALUES ('9', '38', '2022-05-12 09:26:46', '111');
INSERT INTO `vip_info` VALUES ('13', '43', '2022-05-17 15:37:29', '114');
INSERT INTO `vip_info` VALUES ('14', '43', '2022-05-17 23:59:02', '115');
INSERT INTO `vip_info` VALUES ('16', '38', '2022-05-19 15:36:07', '119');
INSERT INTO `vip_info` VALUES ('17', '38', '2022-05-19 16:28:27', '118');
INSERT INTO `vip_info` VALUES ('18', '44', '2022-05-19 16:37:20', '114');
INSERT INTO `vip_info` VALUES ('19', '38', '2022-05-19 17:35:40', '121');
INSERT INTO `vip_info` VALUES ('21', '47', '2022-05-20 23:23:33', '123');
INSERT INTO `vip_info` VALUES ('22', '47', '2022-05-21 00:06:10', '124');
INSERT INTO `vip_info` VALUES ('26', '47', '2022-05-21 07:34:33', '125');
