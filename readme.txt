（1）技术栈：
前端：vue+elementui
后端：nodejs+express
数据库：mysql
（2）运行
①安装好npm、node
②将数据库文件organization.sql导入myql数据库
③用vscode打开项目
④进入server文件夹：
安装依赖：npm install
启动：node server.js
⑤进入admin文件夹：
安装依赖： npm install
启动：npm run dev
⑥进入foreground文件夹：
安装依赖：npm install
启动：npm run dev

（3）本机系统配置：
node版本：16.4.0
npm版本：7.22.0
vue脚手架版本：@vue/cli 4.5.13
mysql版本：8.0.18
